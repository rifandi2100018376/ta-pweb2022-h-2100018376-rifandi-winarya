<?php
$arrnilai=array("fulan"=>80,"fulin"=>90,"fulun"=>75,"falan"=>85);
echo "<B>array sebelum diurutkan</B>";
echo "<PRE>";
print_r($arrnilai);
echo "</PRE>";

asort($arrnilai);
reset($arrnilai);
echo "<B>array setelah diurutkan dengan asort()</B>";
echo "<PRE>";
print_r($arrnilai);
echo "</PRE>";

arsort($arrnilai);
reset($arrnilai);
echo "<B>array setelah diurutkan dengan arsort()</B>";
echo "<PRE>";
print_r($arrnilai);
echo "</PRE>";
?>