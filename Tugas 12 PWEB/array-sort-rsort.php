<?php
$arrnilai=array("fulan"=>80,"fulin"=>90,"fulun"=>75,"falan"=>85);
echo "<B>array sebelum diurutkan</B>";
echo "<PRE>";
print_r($arrnilai);
echo "</PRE>";

sort($arrnilai);
reset($arrnilai);
echo "<B>array setelah diurutkan dengan sort()</B>";
echo "<PRE>";
print_r($arrnilai);
echo "</PRE>";

rsort($arrnilai);
reset($arrnilai);
echo "<B>array setelah diurutkan dengan rsort()</B>";
echo "<PRE>";
print_r($arrnilai);
echo "</PRE>";
?>