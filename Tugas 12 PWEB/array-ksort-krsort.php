<?php
$arrnilai=array("fulan"=>80,"fulin"=>90,"fulun"=>75,"falan"=>85);
echo "<B>array sebelum diurutkan</B>";
echo "<PRE>";
print_r($arrnilai);
echo "</PRE>";

ksort($arrnilai);
reset($arrnilai);
echo "<B>array setelah diurutkan dengan ksort()</B>";
echo "<PRE>";
print_r($arrnilai);
echo "</PRE>";

krsort($arrnilai);
reset($arrnilai);
echo "<B>array setelah diurutkan dengan krsort()</B>";
echo "<PRE>";
print_r($arrnilai);
echo "</PRE>";
?>