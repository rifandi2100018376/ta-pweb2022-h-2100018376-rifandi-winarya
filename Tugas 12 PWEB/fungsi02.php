<?php
//fungsi tanpa return tapi dengan parameter
function cetak_ganjil($awal,$akhir){
    for($i=$awal;$i<$akhir;$i++){
        if($i%2==1){
            echo "$i, ";
        }
    }
}
//panggil fungsi
$a=10;
$b=50;
echo "<B>bilangan ganjil dari $a sampai $b, adalah:</B><BR>";
cetak_ganjil($a,$b);
?>